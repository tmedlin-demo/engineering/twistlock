** This was created in 2021! No promises that it will work. **

Please note that the files in this repo aren’t considered supported by GitLab or under any kind of warranty! 

Transforms from the twistcli, (does not use the Twistlock API)
Takes a directory with the twistlock output, transforms their output into a gitlab friendly format 


In the CI config, there is a job that takes the output of the twistlock scan and puts it through that transform script. The resulting json artifact ends up getting uploaded as a container scanning report, and will be viewable in the security dashboard/vulnerability report. 
The transform script will need to live in the repo where the scans will be run.  

