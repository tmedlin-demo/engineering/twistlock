'use strict'

const fs = require('fs')
const path = require('path')
const debug = require('debug')('a2t:transform')
var imageName;
const gitlabReport = {
  version: '0.1',
  remediations: []
}

// Transforms an Twistlock report into the format expected by GitLab.
// Param dir contains the files in which Twistlock writes its test report
function transform_twistlock (dir) {
  debug(`tranforming Twistlock report into Gitlab report format: ${dir}`)

  const vulns = []

  fs.readdirSync(dir).forEach(file => {
    if (!file.endsWith('.json')) return

    try {
      let content = fs.readFileSync(path.join(dir, file))
      imageName = path.basename(file, '.json')
      const twistlockReport = JSON.parse(content)

      const v = _transform(twistlockReport)
      vulns.push(v)
      //console.log(v);
    } catch (err) {
      debug('Error reading Twistlock report')
      throw err
    }
  })

  return { ...gitlabReport, ...{ vulnerabilities: [].concat(...vulns) } }
}


function _transform (report) {
  debug(`tranforming Twistlock report into Gitlab report format for image: ${report.results[0].id}`)
  
    const distro = report.results[0].distro
    const results = []
  for (const v of report.results[0].vulnerabilities) {
      results.push({
      category: 'container_scanning',
      message: `${v.id} in ${v.packageName}`,
      description: `${v.description}`,
      cve: `${distro}:${v.packageName}:${v.id}`, // ToDo: it's not clear out of which data this field is made up
      severity: _mapSeverity(v.severity),
      confidence: 'Unknown',
      //solution: '',
      scanner: {
        id: 'twistlock',
        name: 'twistlock'
      },
      location: {
        dependency: {
          package: {
            // ToDo: Remove the 'imageFullTag' once the dedupe logic is fixed.
            // Currently, the id prevents unrelated findings from being merged together.
           name:  `v.packageName:imageName`
          },
          version: v.packageVersion
        },
        operating_system: distro,
        image: imageName
      },
      identifiers: [
        {
          type: 'cve',
          name: `${v.id}`,
          value: `${v.id}`,
          url: `${v.link}`
        }
      ],
      links: [
        {
          url: `${v.link}`
        }
      ]
    })
  }
  return results
}


function _mapSeverity (severity) {
  severity = severity.toLowerCase()
  const map = {
    critical: 'Critical',
    high: 'High',
    important: 'High',
    low: 'Low',
    medium: 'Medium',
    moderate: 'Medium',
    negligible: 'Info'
  }
  if (!(severity in map)) {
    debug(`Unknown severity "${severity}" found in Twistlock report. Consider mapping it to a supported severity value.`)
    throw new Error(`Could not map severity ${severity}`)
  }

  return map[severity]
}

module.exports = {
  transform_twistlock
}